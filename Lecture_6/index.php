<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Upload Files</title>
   <link rel="stylesheet" href="style.css">
</head>
<body>
      <div class="container">
         <form action="" method="post" enctype="multipart/form-data">
            <input type="file" name="uploaded_file" accept=".jpg, .txt">
            <br><br>
            <input type="submit" value="Upload File" name="upload">
         </form>
      </div>
      <div class="container">
         <?php
            include "upload.php";
         ?>
      </div>
</body>
</html>