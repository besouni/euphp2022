<?php
    session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Page 1</title>
    <style>
        body{
            padding-left: 200px;
        }
    </style>
</head>
<body>
    <h1>Page 1</h1>
    <?php
        include "menu.php";
    ?>
    <hr>
    <?php
        $x1 = 23;
        echo "x1 = ".$x1;
        $_SESSION["x2"] = 24;
        $_SESSION["x3"] = 25;
        $_SESSION["x4"] = 26;
        echo "<br>";
        echo "Session x2 = ".$_SESSION["x2"];
        echo "<br>";
        echo "Session x3 = ".$_SESSION["x3"];
        echo "<br>";
        echo "Session x4 = ".$_SESSION["x4"];
    ?>
</body>
</html>
