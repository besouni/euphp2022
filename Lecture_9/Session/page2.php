<?php
    session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Page 2</title>
    <style>
        body{
            padding-left: 200px;
        }
    </style>
</head>
<body>
    <h1>Page 2</h1>
    <?php
        include "menu.php";
    ?>
    <hr>
    <?php
        echo "x1 = ".$x1;
        echo "<br>";
        echo "Session x2 = ".$_SESSION["x2"];
        unset($_SESSION["x2"]);
        echo "<br>";
        echo "Session x3 = ".$_SESSION["x3"];
        echo "<br>";
        echo "Session x4 = ".$_SESSION["x4"];
    ?>
</body>
</html>
