<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Page 1</title>
    <style>
        body{
            padding-left: 200px;
        }
    </style>
</head>
<body>
    <h1>Page 1</h1>
    <?php
        include "menu.php";
    ?>
    <hr>
    <?php
        $x1 = 23;
        echo "x1 = ".$x1;
        echo "<br>";
        echo time();
        setcookie("x2", 24, time() + 20, "/");
        echo "<br>";
        echo "Cookie x2 - ".$_COOKIE['x2'];
        setcookie("x3", 25, time() + 200, "/");
        echo "<br>";
        echo "Cookie x3 - ".$_COOKIE['x3'];
    ?>
</body>
</html>
