    <?php
       require_once "mysql/connect.php";
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
       <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>MySQL Queries One to One</title>
       <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="container">
           <?php
                include "blocks/nav.php";
           ?>
           <div class="content">
                <?php
                    if(isset($_GET["nav"])&&$_GET["nav"]=="select")
                    {
                        include "blocks/select.php";
                    }
                    else if(isset($_GET["nav"])&&$_GET["nav"]=="insert")
                    {
                        include "blocks/insert.php";
                    }
                    else if(isset($_GET["nav"])&&$_GET["nav"]=="delete")
                    {
                        include "blocks/delete.php";
                    }
                    else if(isset($_GET["nav"])&&$_GET["nav"]=="edit")
                    {
                        include "blocks/edit.php";
                    }
                    else
                    {
                        include "blocks/home.php";
                    }
                ?>
           </div>
        </div>
    </body>
    </html>