<h1>SELECT</h1>
<hr>
<table class="data-table">
    <tbody>
    <?php
    $select_query = "SELECT id, name, lastname, address FROM users ORDER BY id DESC";
    $result = mysqli_query($connect, $select_query);
    if($result){
        if(mysqli_num_rows($result) > 0){
            $counter = 0;
            while($row = mysqli_fetch_assoc($result)){
                $counter++;
                ?>
                <tr>
                    <td><?=$row['id']?></td>
                    <td><?=$counter?></td>
                    <td><?=$row['name']?></td>
                    <td><?=$row['lastname']?></td>
                    <td><?=$row['address']?></td>
                    <td><a href="?nav=edit&&id=<?=$row['id']?>">Edit</a></td>
                    <td><a href="?nav=delete&&id=<?=$row['id']?>">Delete</a></td>
                </tr>
                <?php
            }
        }else{
            echo "The table is empty!!";
        }
    }
    ?>
    </tbody>
    <thead>
    <tr>
        <th>Id</th>
        <th>Number</th>
        <th>Name</th>
        <th>Lastname</th>
        <th>Address</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <td colspan="7">Num records = <?=mysqli_num_rows($result)?></td>
    </tr>
    </tfoot>
</table>
