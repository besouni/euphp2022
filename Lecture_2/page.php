<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Lecture 2</title>
</head>
<body>
   <?php
      $arr3 = ["student1"=>"Kaxa", "student2"=>"Saba", "student3"=>"Jeni"];
      echo "<pre>";
      print_r($arr3);
      echo "</pre>";
      echo "<hr>";
      echo $arr3["student2"];
      foreach($arr3 as $k=>$el){
         echo "<br>";
         echo $k." - ".$el;
      }
      // $arr1 = [4, 5, 6, 9];
      // $arr2 = [
      //     [3, [4, [3, 4, 11], 5], 6],
      //     [6, 9, 2],
      //     [2, 3],
      //     [4, 5, 2, 0],
      //     [4, 9, 7, 2, 3]     
      // ];
      // echo "<pre>";
      // print_r($arr2);
      // echo "</pre>";
      // echo "<hr>";
      // print_r($arr2[0][1][1]);
      // echo "<br>";
      // echo $arr2[1][1];
      // echo "<br>";
      // echo $arr2[0][2];
      // echo "<br>";
      // echo $arr2[2][1];
      // echo "<br>";
      // echo $arr2[1][2];
      // echo "<br>";
      // echo $arr2[4][2];
   ?>
</body>
</html>