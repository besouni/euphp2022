<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap 5 Website Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
   <style>
        .fakeimg {
            height: 200px;
            background: #aaa;
        }
    </style>
</head>
<body>
<?php
    require_once "blocks/header.php";
    require_once "blocks/topnav.php";
?>
<div class="container mt-1">
    <div class="row">
        <?php
            require_once "blocks/leftnav.php";
            require_once "blocks/content.php";
        ?>
    </div>
</div>
<?php
    require_once "blocks/footer.php";
?>
</body>
</html>
