<?php
   require_once "mysql/connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>MySQL 1</title>
   <link rel="stylesheet" href="style.css">
</head>
<body>
   <div class="content">
      <h1>MySQL 1</h1>
      <hr>
      <table class="data-table">
      <?php
         $select_query = "SELECT id, name, lastname, address FROM users WHERE id>0 ORDER BY name ASC";
         $result = mysqli_query($connect, $select_query);
         // echo "<pre>";
         // var_dump($result);
         // echo "</pre>"; 
         if($result){
            if(mysqli_num_rows($result) > 0){
               echo mysqli_num_rows($result);
               // echo "<hr>";
               // $row = mysqli_fetch_all($result);
               // echo "<pre>";
               //    print_r($row);
               // echo "</pre>";
               // echo "<hr>";
               // while($row = mysqli_fetch_row($result)){
               //    echo "<pre>";
               //       print_r($row);
               //    echo "</pre>";
               // }
               echo "<hr>";
               // $row = mysqli_fetch_array($result);
               // echo "<pre>";
               //    print_r($row);
               // echo "</pre>";
               // while($row = mysqli_fetch_array($result)){
               //    echo "<pre>";
               //       print_r($row);
               //    echo "</pre>";
               // }
               // $row = mysqli_fetch_assoc($result);
               // echo "<pre>";
               //    print_r($row);
               // echo "</pre>";
               // while($row = mysqli_fetch_assoc($result)){
               //    echo "<pre>";
               //       print_r($row);
               //    echo "</pre>";
               // }
               while($row = mysqli_fetch_assoc($result)){
                  // echo "<pre>";
                  //    print_r($row);
                  // echo "</pre>";
               ?>
               <tr>
                  <td><?=$row['id']?></td>
                  <td><?=$row['name']?></td>
                  <td><?=$row['lastname']?></td>
                  <td><?=$row['address']?></td>
               </tr>
               <?php
               }
               // $row = mysqli_fetch_field($result);
               // echo "<pre>";
               //    print_r($row);
               // echo "</pre>";
               // while($row = mysqli_fetch_field($result)){
               //    echo "<pre>";
               //       print_r($row);
               //    echo "</pre>";
               // }
               // $row = mysqli_fetch_object($result);
               // echo "<pre>";
               //    print_r($row);
               // echo "</pre>";
               // while($row = mysqli_fetch_object($result)){
               //    echo "<pre>";
               //       print_r($row);
               //    echo "</pre>";
               // }
            }else{
               echo "The table is empty!!";
            }
         }    
      ?>
      </table>
   </div>
</body>
</html>