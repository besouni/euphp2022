<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Task 3 _ 3</title>
   <link rel="stylesheet" href="style.css">
</head>
<body>
   <?php
      include "functions.php";
   ?>
   <form method="post">
      <div class="captcha">
         <span><?=$captcha?></span>
         <input type="hidden" name="real_captcha" value="<?=$captcha?>">
      </div>
      <div><input type="text" name="captcha"></div>
      <div><button>Check Captcha</button></div>
      <div class="result">
         <span>
         <?php 
            if(isset($_POST['captcha'])){
               echo check_captcha($_POST['captcha']);
            }
         ?>
         </span>
      </div>
   </form>
</body>
</html>