<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>File And Folder</title>
</head>
<body>
      <h1>File</h1>
      <?php
         $myfile = fopen("files/test.txt", "r+");
         // fwrite($myfile, "\nHello World");
         echo filesize("files/test.txt");
         echo "<hr>";
         echo fread($myfile, filesize("files/test.txt"));
         fclose($myfile);
         echo "<hr>";
         $myfile = fopen("files/test.txt", "r");
         echo fgets($myfile);
         echo "<br>";
         echo fgets($myfile);
         echo "<hr>";
         while(!feof($myfile)){
            echo fgets($myfile);
            echo "<br>";
         }
         fclose($myfile);
         echo "<hr>";
         $myfile = fopen("files/test.txt", "r");
         echo fgetc($myfile);
         echo "<br>";
         echo fgetc($myfile);
         echo "<hr>";
         while(!feof($myfile)){
            echo fgetc($myfile);
            echo "<br>";
         }
         fclose($myfile);
      ?>
</body>
</html>